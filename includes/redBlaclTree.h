#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <exception>
#include <algorithm>
#include <windows.h>


template <typename T>
class RedBlackTree {
public:
    size_t size() const { return m_size; }
    void add(const T& vaue);
    bool contain(const T& vaue) const;
    void remove(const T& value);
    void print();
private:
    enum color_t {
        BLACK,
        RED
    };

    struct Node {
        T value {};
        Node* parent = nullptr;
        Node* left_son = nullptr;
        Node* right_son = nullptr;
        color_t color {};
    };

    Node* leftRotate(Node* node);
    Node* rightRotate(Node* node);
    void colorSwap(Node* node);

    void insert(Node* root, Node* node);
    bool find(Node* root, const T& value) const;
    void remove(Node* node, const T& value);
    Node* get_next_node(Node* node);
    void print(Node* node, std::vector<std::pair<size_t, std::pair<T, std::string>>>& v, size_t level);

    Node* m_root = nullptr;
    size_t m_size = 0;
};


template<typename T>
void RedBlackTree<T>::colorSwap(Node* node) {
    if (node->left_son && node->right_son
            && node->color == color_t::BLACK
            && node->left_son->color == color_t::RED
            && node->right_son->color == color_t::RED) {
        node->color = color_t::RED;
        node->left_son->color = color_t::BLACK;
        node->right_son->color = color_t::BLACK;
    }
}


template<typename T>
typename RedBlackTree<T>::Node* RedBlackTree<T>::leftRotate(Node* node) {

    if (node->right_son
            && node->right_son->color == color_t::RED) {
        Node* parent = node->parent;
        Node* right = node->right_son;
        Node* temp = right->left_son;

        right->parent = node->parent;
        right->color = color_t::BLACK;
        right->left_son = node;

        node->color = color_t::RED;
        node->right_son = temp;
        node->parent = right;

        if (!parent) {
            m_root = right;
        } else {
            if(parent->left_son == node) {
                parent->left_son = right;
            } else {
                parent->right_son = right;
            }
        }
        return right;
    }
    return node;
}

template<typename T>
typename RedBlackTree<T>::Node* RedBlackTree<T>::rightRotate(Node* node) {
    if (node->left_son && node->left_son->left_son 
            && node->left_son->color == color_t::RED
            && node->left_son->left_son->color == color_t::RED) {
        Node* parent = node->parent;
        Node* left = node->left_son;
        Node* temp = left->right_son;

        left->parent = node->parent;
        left->color = color_t::BLACK;
        left->right_son = node;

        node->color = color_t::RED;
        node->parent = left;
        node->left_son = temp;
        if (!parent) {
            m_root = left;
        } else {
            if(parent->left_son == node) {
                parent->left_son = left;
            } else {
                parent->right_son = left;
            }
        }
        return left;
    }
    return node;
}

template<typename T>
void RedBlackTree<T>::add(const T& value) {
    if (!m_root) {
        m_root = new Node();
        m_root->color = color_t::BLACK;
        m_root->value = value;
    } else {
        Node* root = m_root;
        Node* node = new Node();
        node->color = color_t::RED;
        node->value = value;
        insert(root, node);
    }
    m_root->color = color_t::BLACK;
}


template<typename T>
void RedBlackTree<T>::insert(Node* root, Node* node) {
    if (node->value < root->value) {
        if (!root->left_son) {
            node->parent = root;
            root->left_son = node;
        } else {
            insert(root->left_son, node);
        }
    } else {
        if (!root->right_son) {
            node->parent = root;
            root->right_son = node;
        } else {
            insert(root->right_son, node);
        }
    }
    root = leftRotate(root);
    root = rightRotate(root);
    colorSwap(root);
}

template<typename T>
bool RedBlackTree<T>::contain(const T& value) const {
    Node* node = m_root;
    return find(node, value);
}

template<typename T>
bool RedBlackTree<T>::find(Node* root, const T& value) const {
    if (root) {
        if (root->value == value) {
            return true;
        }
        if (value < root->value) {
            return find(root->left_son, value);
        }
        return find(root->right_son, value);
    }
    return false;
}

template<typename T>
void RedBlackTree<T>::remove(const T& value) {
    try {
        Node* root = m_root;
        remove(root, value);
    } catch(const std::runtime_error& e) {
        std::cerr << e.what() << '\n';
    }
}

template<typename T>
void RedBlackTree<T>::remove(Node* node, const T& value) {
    if (!node) {
        throw std::runtime_error("Value doens't exists");
    }
    if (node->value == value) {
        Node* parent = node->parent;
        Node* new_node;
        if (node->left_son && !node->right_son) {
            new_node = node->left_son;
            new_node->parent = parent;
        } else if (!node->left_son && node->right_son) {
            new_node = node->right_son;
            new_node->parent = parent;
        } else if (!node->left_son && !node->right_son) {
            new_node = nullptr;
        } else {
            Node* right = node->right_son;
            if (!right->left_son) {
                new_node = right;
                new_node->left_son = node->left_son;
            } else {
                new_node = get_next_node(right);
                new_node->left_son = node->left_son;
                new_node->right_son = node->right_son;
            }
        }
        if (parent) {
            if (parent->left_son == node) {
                parent->left_son = new_node;
            } else {
                parent->right_son = new_node;
            }
        } else {
            m_root = new_node;
        }
        delete node;
    } else if (value < node->value) {
        remove(node->left_son, value);
    } else {
        remove(node->right_son, value);
    }
    node = leftRotate(node);
    node = rightRotate(node);
    colorSwap(node);
}

template <typename T>
typename RedBlackTree<T>::Node* RedBlackTree<T>::get_next_node(Node* node) {
    if (!node->left_son) {
        Node* parent = node->parent;
        parent->left_son = node->right_son;
        return node;
    }
    return get_next_node(node->left_son);
}

template <typename T>
void RedBlackTree<T>::print() {
    std::vector<std::pair<size_t, std::pair<T, std::string>>> v;

    Node* node = m_root;
    print(node, v, 0);

    std::sort(v.begin(), v.end(), [](auto const& left, auto const& right){
        return left.first < right.first;
    });

    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    size_t level = 0;
    for (auto const& el: v) {
        if (level < el.first) {
            std::cout << std::endl;
            level++;
        }
        if (el.second.second == "red") {
            SetConsoleTextAttribute(hConsole, 15 << 4 | 4);
        } else if (el.second.second == "black") {
            SetConsoleTextAttribute(hConsole, 15 << 4 | 0);
        } else {
            SetConsoleTextAttribute(hConsole, 15 << 4 | 15);
        }
        std::cout << el.second.first << ' ' << el.second.second << '\t';
    }
    std::cout << std::endl;
}

template <typename T>
void RedBlackTree<T>::print(Node* node, std::vector<std::pair<size_t, std::pair<T, std::string>>>& v, size_t level) {
    if (!node) {
        v.push_back({level, {{}, ""}});
        return;
    }
    std::string color = node->color == color_t::BLACK ? "black" : "red";
    v.push_back({level, {node->value, color}});
    level++;
    print(node->left_son, v, level);
    print(node->right_son, v, level);
}