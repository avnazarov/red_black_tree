#include "redBlaclTree.h"


int main() {
    RedBlackTree<int> tree;
    tree.add(24);
    tree.print();
    tree.add(5);
    tree.print();
    tree.add(1);
    tree.print();
    tree.add(15);
    tree.print();
    tree.add(3);
    tree.print();
    tree.add(8);
    tree.print();
    std::string exist = tree.contain(8) ? "true" : "false";
    std::cout << exist << std::endl;
    tree.remove(5);
    tree.print();
}